package test;

import mobgen.interviewlibrary.logic.AndroidDevice;
import mobgen.interviewlibrary.logic.Device;
import mobgen.interviewlibrary.logic.IOSDevice;
import org.testng.annotations.*;
import pages.LoginPage;

import java.net.MalformedURLException;

/**
 * Created by marioalvarezmiyar on 30/11/15.
 */
public class LoginPageTest {
    Device device;
    LoginPage loginPage;

    @DataProvider(name="devicesProvider")
    public Object[][] getDevice() {
        return new Object[][]{{new AndroidDevice("BQ", "Aquaris")}, {new IOSDevice("Iphone", "6")}};
    }

    @BeforeMethod
    public void setUp(){
        System.out.println("Executing task before test");
    }


    @Test(dataProvider= "devicesProvider")
    public void loginPageTest(Device device){
        this.device = device;
        loginPage = new LoginPage(device);
        loginPage.setUserEmail("interview@mobgen.com");
        loginPage.execute();
        //Here we should check the behaviour of the app with necessary asserts
    }

    @AfterMethod
    public void tearDown(){
        try {
            //Reset app and release driver
            device.resetApp();
            device.getDriver();
        } catch (MalformedURLException e) {
        }
        device = null;
        loginPage = null;
    }
}
